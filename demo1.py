# -*- coding: utf-8 -*-
"""
Created on Wed Sep 09 10:24:57 2015

@author: gkalmanovich
"""

import time
import numpy as np
import numpy.random as nprnd
import matplotlib.pyplot as pplot

# bubbleSort() from: http://interactivepython.org/runestone/static/pythonds/SortSearch/TheBubbleSort.html
def bubbleSort(alist):
    for passnum in range(len(alist)-1,0,-1):
        for i in range(passnum):
            if alist[i]>alist[i+1]:
                temp = alist[i]
                alist[i] = alist[i+1]
                alist[i+1] = temp


def build_timing_arrays(array_of_n, array_of_timings,function_to_time):
    n_i=128
    for _ in range(15):
        arr = nprnd.randint(0,1000000,n_i)
        array_of_n.append(n_i)
        
        # Timed function
        arr_copy = arr
        start = time.time()
        function_to_time(arr_copy)
        array_of_timings.append(time.time()-start)

        # Check if your function executed correctly (if you are writing your own function)
        #if not function_to_time.check_sort(arr_copy): print "quickSort_slow sort failed"
    
        # Increase the size of the array
        n_i = int(1.25*n_i)

array_of_n = []
array_of_timings = []
build_timing_arrays(array_of_n,array_of_timings,bubbleSort)
#build_timing_arrays(array_of_n,array_of_timings,np.sort)
    
# Plots

pplot.plot(array_of_n,np.array(array_of_timings))
pplot.show()
quit()



pplot.plot(array_of_n,np.array(array_of_timings))
pplot.xscale('log')
pplot.show()
pplot.plot(array_of_n,np.array(array_of_timings)/np.log(np.array(array_of_n))) # log(N)
pplot.xscale('log')
pplot.show()
pplot.plot(array_of_n,np.array(array_of_timings)/(np.array(array_of_n))) # N
pplot.xscale('log')
pplot.show()
pplot.plot(array_of_n,np.array(array_of_timings)/(np.array(array_of_n)*np.log(np.array(array_of_n)))) # N log(N)
pplot.xscale('log')
pplot.show()
pplot.plot(array_of_n,np.array(array_of_timings)/((np.array(array_of_n)*1.0)**2)) # N^2
pplot.xscale('log')
pplot.show()
pplot.plot(array_of_n,np.array(array_of_timings)/((np.array(array_of_n)*1.0)**2*np.log(np.array(array_of_n)))) # N^2 log(N)
pplot.xscale('log')
pplot.show()
pplot.plot(array_of_n,np.array(array_of_timings)/((np.array(array_of_n)*1.0)**3)) # N^3
pplot.xscale('log')
pplot.show()
